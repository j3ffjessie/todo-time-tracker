import './App.css';
import GoalForm from './Components/form';
import Table from './Components/table';

function App() {
  return (
    <div className="App">
      <h1>ToDo/Goal Tracker</h1>
      <GoalForm />
    </div>
  )
};

export default App;
